![logo](images/extralayer10.png)



# [ExtraLayer](https://extralayer.eu/)  

---

## Supported Tools:

[**Duplicati**](./duplicati.md) - Free backup software to store encrypted backups online For Windows, macOS and Linux

[**Rclone**](./rclone.md) - a command-line program to manage files on cloud storage.

[**Rsync**](./rsync.md) - an open source utility that provides fast incremental file transfer.

[**Borg**](./borg.md) - Deduplicating archiver with compression and encryption.

[**SCP**](./scp.md) - OpenSSH secure file copy.

[**SFTP**](./sftp.md) - OpenSSH secure file transfer.

[**SSHFS**](./sshfs.md) -  allows you to mount a remote filesystem using SFTP.

[**ZFS Snaps**](./zfs.md) - OpenZFS is an advanced file system and volume manager.
