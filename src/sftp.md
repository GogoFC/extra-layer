# [SFTP](https://www.openssh.com/)

---

## Using SFTP to transfer data.

### Connect to Server via SFTP

```sh
sftp user@extralayer.eu
```
![con](images/sftpconnect.png)

### List files on remote server

```sh
ls
```
![ls](images/sftpls.png)

### Dowloading a file

```
get filename.txt
```
![get](images/sftpget.png)

### Uploading a flie

```sh
put filename.md
```
![put](images/sftpput.png)

## Other commands

Download a directory and the contents.
```sh
get -r directory
```
Upload a directory and the contents.
```sh
put -r directory
```

Display remote working directory.
```sh
pwd
```
Print local working directory.
```sh
lpwd
```
Display a remote directory listing
```sh
ls
```
Display local directory listing
```sh
lls
```
Quit sftp
```sh
bye
```
![bye](images/sftpbye.png)


# Docs

[Manual Page - SFTP](https://man.openbsd.org/sftp.1)
