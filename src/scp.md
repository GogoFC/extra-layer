# [SCP](https://www.openssh.com/)

---

## Using SCP to copy data.


Copy a directory and it's contents recursively. 


```bash
sudo scp -r /home/user/data user@10.0.0.1:/home/user
```

- SCP does not support resuming if the transfer is interrupted. 

- If a file only got transferred partially it can be resumed with [Rsync](./rsync.md) using the `-P` flag or [SFTP](./sftp.md) using the `-a` flag.


# Docs

[Manual Page - SCP](https://man.openbsd.org/scp.1)
